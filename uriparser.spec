Name:           uriparser
Version:        0.9.8
Release:        2
Summary:        A strictly RFC 3986 compliant URI parsing and handling library written in C89
License:        BSD-3-Clause
URL:            https://uriparser.github.io/
Source0:        https://github.com/uriparser/uriparser/releases/download/uriparser-%{version}/uriparser-%{version}.tar.bz2

BuildRequires:  cmake doxygen gcc-c++ graphviz gtest-devel make gmock

%description
The package is a strictly RFC 3986 compliant URI parsing library written in C89("ANSI C").
uriparser is cross-platform, fast, supports Unicode and is licensed under the New BSD license.
There are a number of applications, libraries and hardware using uriparser, as well as bindings
and 3rd-party wrappers. uriparser is packaged in major distributions.

%package        devel
Summary:        Files for uriparser development
Requires:       uriparser = %{version}-%{release}
Conflicts:      uriparser < 0.9.8-2

%description    devel
The package contains libraries and header files for developing applications that use uriparser.

%package        help
Summary:        Documentation for uriparser
Provides:       uriparser-doc = %{version}-%{release}
Obsoletes:      uriparser-doc < %{version}-%{release}
BuildArch:      noarch

%description    help
The package contains HTML documentation files for uriparser.

%prep
%autosetup -p1
sed -i 's/GENERATE_QHP\ =\ yes/GENERATE_QHP\ =\ no/g' doc/Doxyfile.in

%build
%cmake
%cmake_build

%install
%cmake_install

%check
%ctest

%files
%doc THANKS AUTHORS ChangeLog COPYING
%{_bindir}/uriparse
%{_libdir}/liburiparser.so.1
%{_libdir}/liburiparser.so.1.*

%files devel
%{_includedir}/uriparser
%{_libdir}/*.so
%{_libdir}/cmake/%{name}-%{version}
%{_libdir}/pkgconfig/lib%{name}.pc

%files help
%doc %{_docdir}/uriparser/html

%changelog
* Wed Nov 06 2024 Funda Wang <fundawang@yeah.net> - 0.9.8-2
- adopt to new cmake macro

* Fri Aug 2 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.9.8-1
- update to version 0.9.8

* Mon May 6 2024 kouwenqi <kouwenqi@kylinos.cn> - 0.9.7-2
- fix CVE-2024-34402,CVE-2024-34403

* Fri Sep 15 2023 yaoxin <yao_xin001@hoperun.com> - 0.9.7-1
- Update to 0.9.7

* Thu Apr 14 2022 wangkai <wangkai385@h-partners.com> - 0.9.6-2
- Add BuildRequires to fix build error and add uriparser.yaml

* Tue Jan 25 2022 wangkai <wangkai385@huawei.com> - 0.9.6-1
- Update to 0.9.6 for fix CVE-2021-46141 and CVE-2021-46142

* Thu Mar 5 2020 Ling Yang <lingyang2@huawei.com> - 0.9.3-2
- Package Init
